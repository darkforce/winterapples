#!/bin/bash

BUILDFOLDER="./build/"

npm run build
rm -fr $BUILDFOLDER
mkdir -p $BUILDFOLDER
cp -r ./dist/* $BUILDFOLDER
cp info.html $BUILDFOLDER
cp -r assets $BUILDFOLDER
rm ./build.tar.gz
tar czvf ./build.tar.gz $BUILDFOLDER

