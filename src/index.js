import Phaser from './phaser-custom';
import GameScene from './scenes/GameScene'
import GameOverScene from './scenes/GameOverScene'

const config = {
	type: Phaser.AUTO,
	parent: 'game',
	width: 800,
	height: 400,
	scale: {
		mode: Phaser.Scale.FIT,
		autoCenter: Phaser.Scale.CENTER_BOTH,
	},
	pixelArt: true,
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {y: 500},
			debug: false,
		}
	},
	//audio: {
		//disableWebAudio: true,
		//noAudio: true,
	//},
	scene: [GameScene, GameOverScene]
};

export default new Phaser.Game(config);

