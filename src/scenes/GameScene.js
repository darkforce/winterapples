import Phaser from '../phaser-custom';
import Horse from '../objects/horse';
import Snowflakes from '../objects/snowflakes';
import Ground from '../objects/ground';
import Bells from '../objects/bells';
import ButtonFullscreen from '../objects/buttonFullscreen.js';

export default class GameScene extends Phaser.Scene {
	constructor() {
		super('game-scene');

		this.score = 0;
		this.bellPoints = 10;
		this.highscore = 0;

		this.wasStanding = true;
		this.lastDirection = 0;

		this.initialMovement = false;

		this.deathZoneOffset = 350;

		this.isGameOver = false;
		this.gameOverScreenLaunched = false;
		this.newHighscore = false;

		this.loadHighscoreFromStorage();
	}

	loadHighscoreFromStorage() {
		var lsHighscore = localStorage.getItem('ws_highscore');
		if (lsHighscore !== null) {
			this.highscore = lsHighscore;
		}
	}

	saveHighscoreToStorage() {
		localStorage.setItem('ws_highscore', this.highscore);
	}

	preload() {
		this.load.spritesheet('imgHorse', 'assets/images/horse.png', {frameWidth: 80, frameHeight: 52});
		this.load.image('imgGround', 'assets/images/ground.png');
		this.load.image('imgBackground', 'assets/images/bg.png');
		this.load.image('imgApple', 'assets/images/apple.png');
		this.load.image('imgCarrot', 'assets/images/carrot.png');
		this.load.spritesheet('imgSnowflake', 'assets/images/snowflake.png', {frameWidth: 5, frameHeight: 5});
		this.load.spritesheet('imgBtnFullscreen', 'assets/images/button_fullscreen.png', {frameWidth: 32, frameHeight: 32});
		this.load.spritesheet('imgBtnInfo', 'assets/images/button_info.png', {frameWidth: 32, frameHeight: 32});

		this.load.audio('sfxBell', ['assets/sounds/bell.ogg', 'assets/sounds/bell.mp3']);
		this.load.audio('sfxBird', ['assets/sounds/crunch.ogg', 'assets/sounds/crunch.mp3']);
	}

	create() {
		this.player = new Horse(this, 320, 350);
		this.cameras.main.startFollow(this.player);
		
		this.ground = new Ground(this);
		this.physics.add.collider(this.player, this.ground, this.touchingGround, null, this);

		this.bells = new Bells(this, this.maxBells);
		this.physics.add.overlap(this.player, this.bells, this.bellCollision, null, this);

		const sizeY = -this.bells.maxBells*this.bells.heightOffset+1000;
		this.cameras.main.setBounds(0, -sizeY, 800, sizeY+400);
		this.setBackgroundByHeight();
		this.physics.world.setBounds(0, -sizeY, 800, sizeY+400);

		this.snowflakes = new Snowflakes(this);

		this.textPoints = this.add.text(10, 0);
		this.textPoints.setScrollFactor(0);
		this.textPoints.setFontFamily('m5x7');
		this.textPoints.setFontSize('32px');
		this.displayPoints();

		this.input.on('pointerdown', function() { this.jump(); }, this);

		this.deathZone = this.add.zone(400, 500, 800, 10);
		this.physics.world.enable(this.deathZone, 0);
		this.deathZone.body.setAllowGravity(false);
		this.physics.add.overlap(this.player, this.deathZone, this.deathCollision, null, this);

		this.btnFullscreen = new ButtonFullscreen(this, 770, 30);
	}

	update(time, delta) {
		this.snowflakes.update(time, delta);

		if (!this.initialMovement) {
			if ((this.input.x != 0) || (this.input.y != 0)) {
				this.initialMovement = true;
			} else {
				return;
			}
		}

		this.player.update(time, delta);
		this.bells.update(time, delta);

		const newDeathZoneHeight = this.player.body.y+this.deathZoneOffset;
		if (newDeathZoneHeight < this.deathZone.y) {
			this.deathZone.y = newDeathZoneHeight;
		}

		const maxHeight = -this.bells.oldBellsOffset*this.bells.heightOffset+(this.bells.maxBells-this.bells.oldBellsOffset)*this.bells.heightOffset;
		if (this.player.body.y < maxHeight) {
			//console.log('WRAP');
			this.nextScreen(maxHeight);

			const newPlayerPositionY =  this.player.y-maxHeight
			this.deathZone.y = newPlayerPositionY+this.deathZoneOffset;
			this.player.body.y = newPlayerPositionY;
		}
	}

	jump() {
		if (this.gameOverScreenLaunched) {
			this.resetGame();
		}

		if (this.initialMovement) {
			this.player.jumpFromGround();
		}
	}

	bellCollision(player, bell) {
		this.player.jumpFromBell();
		if (this.bells.bellCollision(player, bell, this.bellPoints)) {
			this.doublePoints();
		} else {
			this.increasePoints();
		}

		this.setBackgroundByHeight();
	}

	increasePoints() {
		this.score += this.bellPoints;
		this.bellPoints += 10;
		this.displayPoints();
	}

	doublePoints() {
		this.score *= 2;
		this.displayPoints();
	}

	displayPoints() {
		this.textPoints.setText('Score: ' + this.score);
	}

	touchingGround(player, ground) {
		this.player.isTouchingGround = true;
		if (!this.player.wasTouchingGround) {
			this.player.landed();
			this.player.lastDirection = 0;
		}

		if (this.isGameOver && !this.gameOverScreenLaunched) {
			this.scene.launch('gameover-scene', {score: this.score, highscore: this.highscore, newHighscore: this.newHighscore});
			this.gameOverScreenLaunched = true;
		}
	}

	nextScreen(height) {
		this.ground.hideGround();
		this.bells.nextScreen(height);
		this.snowflakes.wrapScreen(height);
	}

	deathCollision(player, zone) {
		this.gameOver();
	}

	gameOver() {
		if (!this.isGameOver) {
			this.isGameOver = true;
			this.bells.deactivateAllBells();

			if (this.score > this.highscore) {
				this.highscore = this.score;
				this.newHighscore = true;
				this.saveHighscoreToStorage();
			}

			if (this.bellPoints > 50) {
				var newGroundPosition = 400+this.bellPoints*(this.bells.heightOffset*-0.1);
				if (newGroundPosition > 10000) {
					newGroundPosition = 10000;
				}
				this.ground.move(newGroundPosition);

				const sizeY = -this.bells.maxBells*this.bells.heightOffset+1000;
				this.cameras.main.setBounds(0, -sizeY, 800, newGroundPosition+sizeY);
				this.physics.world.setBounds(0, -sizeY, 800, newGroundPosition+sizeY);
			}

			this.ground.showGround();
		}
	}

	resetGame() {
		this.bellPoints = 10;
		this.score = 0;
		this.isGameOver = false;
		this.newHighscore = false;
		this.gameOverScreenLaunched = false;
		this.scene.stop('gameover-scene');
		this.scene.restart();
	}

	setBackgroundByHeight() {
		var scale = 0.1;
		if (this.bellPoints < 5000) {
			scale = (1.0-this.bellPoints/5000.)+0.1
		}

		var r = 44*scale;
		var g = 65*scale;
		var b = 114*scale;
		//var color = new Phaser.Display.RGB(r, g, b);
		var color = Phaser.Display.Color.GetColor(r, g, b);
		this.cameras.main.setBackgroundColor(color);
	}
}

