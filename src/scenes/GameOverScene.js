import Phaser from '../phaser-custom';
import ButtonInfo from '../objects/buttonInfo';

export default class GameOverScene extends Phaser.Scene {
	constructor() {
		super('gameover-scene');
	}

	init(data) {
		this.score = data.score;
		this.highscore = data.highscore;
		this.newHighscore = data.newHighscore;
	}
	
	create() {
		this.background = this.add.graphics({x: 100, y: 50});
		this.background.fillStyle('0x000000', 0.85);
		this.background.fillRoundedRect(0, 0, 600, 300, 20);

		this.textGameOver = this.add.text(400, 120);
		this.textGameOver.setOrigin(0.5, 0.5);
		this.textGameOver.setFontFamily('Round9x13');
		this.textGameOver.setFontSize('32px');
		this.textGameOver.setText('GAME OVER');

		if (this.newHighscore) {
			this.textNewHighscore = this.add.text(400, 190);
			this.textNewHighscore.setOrigin(0.5, 0.5);
			this.textNewHighscore.setFontFamily('Round9x13');
			this.textNewHighscore.setFontSize('32px');
			this.textNewHighscore.setText('New Highscore!');
		}

		this.textScore = this.add.text(140, 250);
		this.textScore.setFontSize('32px');
		this.textScore.setFontFamily('m5x7');
		this.textScore.setText('Score:     ' + this.score);
		this.textHighscore = this.add.text(140, 280);
		this.textHighscore.setFontSize('32px');
		this.textHighscore.setFontFamily('m5x7');
		this.textHighscore.setText('Highscore: ' + this.highscore);

		this.btnInfo = new ButtonInfo(this, 770, 370);
	}
}

