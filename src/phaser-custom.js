/**
 * @author       Richard Davey <rich@photonstorm.com>
 * @copyright    2020 Photon Storm Ltd.
 * @license      {@link https://opensource.org/licenses/MIT|MIT License}
 */


require('../node_modules/phaser/src/polyfills');

var CONST = require('../node_modules/phaser/src/const');
var Extend = require('../node_modules/phaser/src/utils/object/Extend');

var Phaser = {
	Actions: require('../node_modules/phaser/src/actions'),

	Cameras: {
		Scene2D: require('../node_modules/phaser/src/cameras/2d')
	},

	Core: require('../node_modules/phaser/src/core'),
	Class: require('../node_modules/phaser/src/utils/Class'),
	Create: require('../node_modules/phaser/src/create'),
	Curves: require('../node_modules/phaser/src/curves'),
	Data: require('../node_modules/phaser/src/data'),
	Display: require('../node_modules/phaser/src/display'),
	DOM: require('../node_modules/phaser/src/dom'),
	Events: require('../node_modules/phaser/src/events/index'),
	Game: require('../node_modules/phaser/src/core/Game'),

	GameObjects: {
		DisplayList: require('../node_modules/phaser/src/gameobjects/DisplayList'),
		UpdateList: require('../node_modules/phaser/src/gameobjects/UpdateList'),

		Image: require('../node_modules/phaser/src/gameobjects/image/Image'),
		Sprite: require('../node_modules/phaser/src/gameobjects/sprite/Sprite'),
		Group: require('../node_modules/phaser/src/gameobjects/group/Group'),
		//Text: require('../node_modules/phaser/src/gameobjects/text/Text'),
		Text: require('../node_modules/phaser/src/gameobjects/text/static/Text'),
		Particles: require('../node_modules/phaser/src/gameobjects/particles'),
		Zone: require('../node_modules/phaser/src/gameobjects/zone/Zone'),
		Graphics: require('../node_modules/phaser/src/gameobjects/graphics/Graphics'),

		Factories: {
			Image: require('../node_modules/phaser/src/gameobjects/image/ImageFactory'),
			Sprite: require('../node_modules/phaser/src/gameobjects/sprite/SpriteFactory'),
			Group: require('../node_modules/phaser/src/gameobjects/group/GroupFactory'),
			//Text: require('../node_modules/phaser/src/gameobjects/text/TextFactory'),
			Text: require('../node_modules/phaser/src/gameobjects/text/static/TextFactory'),
			Particles: require('../node_modules/phaser/src/gameobjects/particles/ParticleManagerFactory'),
			Zone: require('../node_modules/phaser/src/gameobjects/zone/ZoneFactory'),
			Graphics: require('../node_modules/phaser/src/gameobjects/graphics/GraphicsFactory'),
		},

		Creators: {
			Image: require('../node_modules/phaser/src/gameobjects/image/ImageCreator'),
			Sprite: require('../node_modules/phaser/src/gameobjects/sprite/SpriteCreator'),
			Group: require('../node_modules/phaser/src/gameobjects/group/GroupCreator'),
			//Text: require('../node_modules/phaser/src/gameobjects/text/TextCreator'),
			Text: require('../node_modules/phaser/src/gameobjects/text/static/TextCreator'),
			Particles: require('../node_modules/phaser/src/gameobjects/particles/ParticleManagerCreator'),
			Zone: require('../node_modules/phaser/src/gameobjects/zone/ZoneCreator'),
			Graphics: require('../node_modules/phaser/src/gameobjects/graphics/GraphicsCreator'),
		}
	},
	
	Loader: {
		FileTypes: {
			ImageFile: require('../node_modules/phaser/src/loader/filetypes/ImageFile'),
			SpriteSheetFile: require('../node_modules/phaser/src/loader/filetypes/SpriteSheetFile'),
			HTML5AudioFile: require('../node_modules/phaser/src/loader/filetypes/HTML5AudioFile'),
			AudioFile: require('../node_modules/phaser/src/loader/filetypes/AudioFile'),
		},
		LoaderPlugin: require('../node_modules/phaser/src/loader/LoaderPlugin')
	},
	Math: {
		Between: require('../node_modules/phaser/src/math/Between')
	},
	Physics: {
		Arcade: require('../node_modules/phaser/src/physics/arcade')
	},
	Scenes: require('../node_modules/phaser/src/scene'),


	Input: require('../node_modules/phaser/src/input'),
	Scale: require('../node_modules/phaser/src/scale'),
	Scene: require('../node_modules/phaser/src/scene/Scene'),
	Time: require('../node_modules/phaser/src/time'),
};

if (typeof FEATURE_SOUND)
{
	Phaser.Sound = require('../node_modules/phaser/src/sound');
}

//   Merge in the consts
Phaser = Extend(false, Phaser, CONST);

//  Export it
module.exports = Phaser;
global.Phaser = Phaser;

