import Phaser from '../phaser-custom';

export default class ButtonFullscreen extends Phaser.GameObjects.Sprite {
	constructor(scene, x, y) {
		super(scene, x, y, 'imgBtnInfo', 0);

		this.scene = scene;
		this.scene.add.existing(this);

		this.setScrollFactor(0);
		
		this.createEventHandlers();

		this.url = './info.html';
	}

	createEventHandlers() {
		this.setInteractive();

		this.on('pointerover', this.mouseOver, this);
		this.on('pointerout', this.mouseOut, this);
		this.on('pointerdown', this.mouseDown, this);
	}

	mouseOver(pointer) {
		this.setFrame(1);
	}

	mouseOut(pointer) {
		this.setFrame(0);
	}

	mouseDown(pointer) {
		var newWindow = window.open(this.url, '_blank');

		if (newWindow && newWindow.focus) {
			newWindow.focus();
		} else {
			window.location.href = this.url;
		}
	}
}

