import Phaser from '../phaser-custom';

export default class Horse extends Phaser.GameObjects.Sprite {
	constructor(scene, x, y) {
		super(scene, x, y, 'imgHorse', 1);

		this.scene = scene;

		this.scene.physics.world.enable(this);
		this.scene.add.existing(this);

		this.setDepth(1);

		this.body.setBounce(0.0);
		this.body.setCollideWorldBounds(true);
		this.body.setSize(45, 30);
		this.body.setOffset(17, 22);

		this.createAnimations();

		this.isTouchingGround = false;
		this.wasTouchingGround = false;

		this.groundJumpVelocity = -500;
		this.bellJumpVelocity = -400;
	}

	createAnimations() {
		this.scene.anims.create({
			key: 'walk_left',
			frames: this.scene.anims.generateFrameNumbers('imgHorse', {start: 35, end: 43}),
			frameRate: 10,
			repeat: -1
		});
		
		this.scene.anims.create({
			key: 'walk_right',
			frames: this.scene.anims.generateFrameNumbers('imgHorse', {start: 10, end: 18}),
			frameRate: 10,
			repeat: -1
		});

		this.scene.anims.create({
			key: 'jump_left',
			frames: this.scene.anims.generateFrameNumbers('imgHorse', {frames: [47, 48, 49]}),
			frameRate: 15,
			repeat: 0 
		});
		
		this.scene.anims.create({
			key: 'jump_right',
			frames: this.scene.anims.generateFrameNumbers('imgHorse', {frames: [22, 23, 24]}),
			frameRate: 10,
			repeat: 0 
		});

		this.scene.anims.create({
			key: 'flying_left',
			frames: this.scene.anims.generateFrameNumbers('imgHorse', {frames: [49]}),
			frameRate: 10,
			repeat: 0 
		});
		
		this.scene.anims.create({
			key: 'flying_right',
			frames: this.scene.anims.generateFrameNumbers('imgHorse', {frames: [24]}),
			frameRate: 10,
			repeat: 0 
		});
		
		this.scene.anims.create({
			key: 'stand_left',
			frames: this.scene.anims.generateFrameNumbers('imgHorse', {start: 25, end: 34}),
			frameRate: 5,
			repeat: -1,
			repeatDelay: 3000
		});

		this.scene.anims.create({
			key: 'stand_right',
			frames: this.scene.anims.generateFrameNumbers('imgHorse', {start: 0, end: 9}),
			frameRate: 5,
			repeat: -1,
			repeatDelay: 3000
		});

	}

	update(time, delta) {
		const diffX = this.scene.input.x - this.x;
		const diffXAbs = Math.abs(diffX);
		if (diffXAbs > 0.1) {
			var velocity = 0;
			const signum = Math.sign(diffX);

			if (!this.isTouchingGround) {
				if (diffXAbs > 75) {
					velocity = signum*750;
				} else {
					velocity = diffX*10;
				}
			} else {
				if (diffXAbs > 50) {
					velocity = signum*250;
				} else {
					velocity = diffX*5;
				}
			}

			this.body.setVelocityX(velocity);

			if (this.wasStanding || (this.lastDirection != signum)) {
				if (this.isTouchingGround) {
					if (signum > 0) {
						this.anims.play('walk_right');
					} else {
						this.anims.play('walk_left');
					}
				} else {
					if (signum > 0) {
						this.anims.play('jump_right');
					} else {
						this.anims.play('jump_left');
					}
				}

				this.wasStanding = false;
				this.lastDirection = signum;
			}
		} else {
			this.body.setVelocityX(0);

			if (!this.wasStanding) {
				if (this.isTouchingGround) {
					if (this.lastDirection > 0) {
						this.anims.play('stand_right');
					} else {
						this.anims.play('stand_left');
					}
				} else {
					if (this.lastDirection > 0) {
						this.anims.play('flying_right');
					} else {
						this.anims.play('flying_left');
					}
				}

				this.wasStanding = true;
			}
		}

		this.wasTouchingGround = this.isTouchingGround;
		this.isTouchingGround = false;
	}

	jumpFromGround() {
		if (this.wasTouchingGround) {
			this.body.setVelocityY(this.groundJumpVelocity);

			if (this.lastDirection > 0) {
				this.anims.play('jump_right');
			} else {
				this.anims.play('jump_left');
			}
		}
	}

	jumpFromBell() {
		this.body.setVelocityY(this.bellJumpVelocity);

		if (this.lastDirection > 0) {
			this.anims.play('jump_right');
		} else {
			this.anims.play('jump_left');
		}
	}

	landed() {
		this.wasTouchingGround = true;

		if (this.lastDirection > 0) {
			this.anims.play('stand_right');
		} else {
			this.anims.play('stand_left');
		}
	}
}


