import Phaser from '../phaser-custom';

export default class Snowflakes extends Phaser.Physics.Arcade.Group {
	constructor(scene, numberOfSnowflakes=100) {
		super(scene.physics.world, scene, {allowGravity: false});

		this.scene = scene;

		this.createSnowflakes(numberOfSnowflakes);
	}

	createSnowflakes(numberOfSnowflakes) {
		this.numberOfSnowflakes = numberOfSnowflakes;

		for (var i = 0; i < numberOfSnowflakes; ++i) {
			const posX = Phaser.Math.Between(0, 800);
			const posY = Phaser.Math.Between(0, 400);
			const velX = Phaser.Math.Between(-3, 3);
			const velY = Phaser.Math.Between(25, 75);
			const snowflakeType = Phaser.Math.Between(0, 7);
			this.create(posX, posY, 'imgSnowflake', snowflakeType).setVelocityY(velY).setVelocityX(velX).setDepth(-2);
		}
	}

	update(time, delta) {
		var cameraHeight = this.scene.cameras.main.displayHeight;
		var cameraWidth = this.scene.cameras.main.displayWidth;
		var upper = this.scene.cameras.main.scrollY;
		var lower = this.scene.cameras.main.scrollY+cameraHeight;
		var left = this.scene.cameras.main.scrollX;
		var right = this.scene.cameras.main.scrollX+cameraWidth;

		Phaser.Actions.Call(this.getChildren(), function(snowflake) { 
			if (snowflake.y > lower) {
				snowflake.y -= cameraHeight;
			} else if (snowflake.y < upper) {
				snowflake.y += cameraHeight;
			}

			if (snowflake.x > right) {
				snowflake.x -= cameraWidth;
			} else if (snowflake.x < left) {
				snowflake.x += cameraWidth;
			}
		}, this);
	}

	wrapScreen(height) {
		Phaser.Actions.Call(this.getChildren(), function(snowflake) { 
			snowflake.y -= height;
		}, this);
	}
}

