import Phaser from '../phaser-custom';

export default class Ground extends Phaser.Physics.Arcade.StaticGroup {
	constructor(scene) {
		super(scene.physics.world, scene);

		this.scene = scene;

		this.mainGround = this.create(0, 400, 'imgGround');
		this.mainGround.setOrigin(0, 1);
		this.mainGround.setDepth(-1);
		this.mainGround.refreshBody();

		this.background = this.scene.add.sprite(0, 400, 'imgBackground');
		this.background.setOrigin(0, 1);
		this.background.setDepth(-3);
	}

	update(time, delta) {

	}

	showGround() {
		this.mainGround.enableBody(false, 0, 0, true, true);
		this.background.setVisible(true);
	}

	hideGround() {
		this.mainGround.disableBody(true, true);
		this.background.setVisible(false);
	}

	move(y) {
		this.mainGround.y = y;
		this.mainGround.refreshBody();
		this.background.y = y;
	}
}

