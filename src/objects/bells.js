import Phaser from '../phaser-custom';

export default class Bells extends Phaser.Physics.Arcade.Group {
	constructor(scene, maxBells=40) {
		super(scene.physics.world, scene, {allowGravity: false});

		this.scene = scene;

		this.maxBells = maxBells

		this.bellPositions = new Array(this.maxBells);
		this.bellObjects = new Array(this.maxBells);

		this.globalOffset = 200;
		this.heightOffset = -90;
		this.oldBells = 6;
		this.oldBellsOffset = this.oldBells/2;

		this.createBells();
		this.createText();
		this.createParticleEmitter();
		this.createAudio();
	}

	createText() {
		this.text = new Phaser.GameObjects.Text(this.scene, 0, 0);
		this.text.setVisible(false);
		this.text.setText('10');
		this.text.setFontFamily('m5x7');
		this.text.setFontSize('32px');
		this.text.setOrigin(0.5, 0.5);

		this.scene.add.existing(this.text);

		this.textTimer = null;
	}

	createParticleEmitter() {
		this.particleEmitterManagerBell = new Phaser.GameObjects.Particles.ParticleEmitterManager(this.scene, 'imgApple');
		this.scene.add.existing(this.particleEmitterManagerBell);

		this.particleEmitterBell = this.particleEmitterManagerBell.createEmitter({
			x: 0,
			y: 0,
			speed: {min: -400, max: 400},
			angle: {min: 0, max: 360},
			scale: {start: 0.5, end: 0},
			blendMode: 'SCREEN',
			active: true,
			lifespan: 400,
			on: false
		});

		this.particleEmitterManagerBird = new Phaser.GameObjects.Particles.ParticleEmitterManager(this.scene, 'imgCarrot');
		this.scene.add.existing(this.particleEmitterManagerBird);

		this.particleEmitterBird = this.particleEmitterManagerBird.createEmitter({
			x: 0,
			y: 0,
			speed: {min: -400, max: 400},
			angle: {min: 0, max: 360},
			scale: {start: 0.5, end: 0},
			blendMode: 'SCREEN',
			active: true,
			lifespan: 400,
			on: false
		});
	}

	createBells() {
		for (var bellNr = 0; bellNr < this.maxBells; ++bellNr) {
			const posX = Phaser.Math.Between(20, 780);
			const posY = this.globalOffset+bellNr*this.heightOffset;

			if ((bellNr+1) % 25) {
				var newBell = this.addBell(posX, posY);
			} else {
				var newBell = this.addBird(posX, posY);
			}

			this.bellPositions[bellNr] = posX;
			this.bellObjects[bellNr] = newBell;
		}
	}

	createAudio() {
		this.sfxBell = this.scene.sound.add('sfxBell');
		this.sfxBird = this.scene.sound.add('sfxBird');
	}

	addBell(x, y) {
		var newBell = new Bell(this.scene, x, y);
		this.add(newBell);

		return newBell;
	}

	addBird(x, y) {
		var newBird = new Bird(this.scene, x, y);
		this.add(newBird);
		newBird.resetPhysics();

		return newBird;
	}
	
	update(time, delta) {
	}

	bellCollision(player, bell, text) {
		bell.wasHit();

		if (bell.isBird) {
			text = 'Score x2';
			this.sfxBird.play();
		} else {
			this.sfxBell.play();
		}

		this.showText(text, bell.x, bell.y);
		this.showParticles(bell.x, bell.y, bell.isBird);

		return bell.isBird;
	}

	showText(text, x, y) {
		this.text.setText(text);
		this.text.setPosition(x, y);
		this.text.setVisible(true);
		
		if (this.textTimer !== null) {
			this.textTimer.remove();
		}
		this.textTimer = this.scene.time.delayedCall(1000, this.hideText, null, this);
	}

	showParticles(x, y, isBird) {
		if (isBird) {
			this.particleEmitterBird.explode(20, x, y)
		} else {
			this.particleEmitterBell.explode(20, x, y)
		}
	}

	hideText() {
		this.text.setVisible(false);
	}

	nextScreen(height) {
		for (var bellNr = 0; bellNr < this.oldBells; ++bellNr) {
			const oldNr = this.maxBells-this.oldBells+bellNr;
			this.bellPositions[bellNr] = this.bellPositions[oldNr];
			if (this.bellObjects[oldNr].body.enable) {
				this.bellObjects[bellNr].activateBell(this.bellPositions[bellNr], this.globalOffset+this.heightOffset*bellNr);
				this.bellObjects[bellNr].resetPhysics();
			} else {
				this.bellObjects[bellNr].deactivateBell();
			}
		}
		
		for (var bellNr = this.oldBells; bellNr < this.maxBells; ++bellNr) {
			const posX = Phaser.Math.Between(20, 780);
			this.bellPositions[bellNr] = posX;
			this.bellObjects[bellNr].activateBell(posX);
			this.bellObjects[bellNr].resetPhysics();
		}

		this.text.setPosition(this.text.x, this.text.y-height);
	}

	deactivateAllBells() {
		for (var bellNr = 0; bellNr < this.maxBells; ++bellNr) {
			this.bellObjects[bellNr].deactivateBell();
		}
	}
}

export class Bell extends Phaser.Physics.Arcade.Sprite {
	constructor(scene, x, y, key) {
		if (key === undefined) {
			key = 'imgApple';
		}
		super(scene, x, y, key);

		scene.add.existing(this);
		scene.physics.add.existing(this);

		this.scene = scene;
		this.isBird = false;
	}

	resetPhysics() {
	}

	activateBell(x, y) {
		if (y === undefined) {
			y = this.y;
		}

		this.enableBody(true, x, y, true, true);
	}

	deactivateBell() {
		this.disableBody(true, true);
	}

	wasHit() {
		this.deactivateBell();
	}
}

export class Bird extends Bell {
	constructor(scene, x, y) {
		super(scene, x, y, 'imgCarrot');

		scene.add.existing(this);

		this.scene = scene;
		this.isBird = true;
	}

	resetPhysics() {
		this.setVelocityX(100);
		this.setCollideWorldBounds(true);
		this.setBounce(1.0);
	}
}

