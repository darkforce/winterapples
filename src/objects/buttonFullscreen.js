import Phaser from '../phaser-custom';

export default class ButtonFullscreen extends Phaser.GameObjects.Sprite {
	constructor(scene, x, y) {
		super(scene, x, y, 'imgBtnFullscreen', 0);

		this.scene = scene;
		this.scene.add.existing(this);

		this.setScrollFactor(0);
		
		this.createEventHandlers();
	}

	createEventHandlers() {
		this.setInteractive();

		this.on('pointerover', this.mouseOver, this);
		this.on('pointerout', this.mouseOut, this);
		this.on('pointerdown', this.mouseDown, this);
	}

	mouseOver(pointer) {
		if (this.scene.scale.isFullscreen) {
			this.setFrame(3);
		} else {
			this.setFrame(1);
		}
	}

	mouseOut(pointer) {
		if (this.scene.scale.isFullscreen) {
			this.setFrame(2);
		} else {
			this.setFrame(0);
		}
	}

	mouseDown(pointer) {
		if (this.scene.scale.isFullscreen) {
			this.scene.scale.stopFullscreen();
		} else {
			this.scene.scale.startFullscreen();
		}
	}
}

