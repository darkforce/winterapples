The software and all assets not explicitly listed are licensed under the MIT license (see text below).

#### Software
[Phaser](https://phaser.io/) by [Richard Davey, Photon Storm Ltd.](http://www.photonstorm.com/) is licensed under the [MIT License](https://phaser.io/download/license).

### Assets under different licenses: 

#### Fonts
[m5x7](https://managore.itch.io/m5x7/) by [Daniel Linssen](https://managore.itch.io/) is licensed under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/legalcode).

[Round9x13](https://heraldod.itch.io/bitmap-fonts) by [Herald](https://heraldod.itch.io/) is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode).

#### Images
[Pine Tree Pack](https://opengameart.org/content/pine-tree-pack) by [KnoblePersona](https://opengameart.org/users/knoblepersona) is licensed under [CC-BY 3.0](https://creativecommons.org/licenses/by/3.0/legalcode).

[Horse Sprite with Rider](https://onfe.itch.io/horse-sprite-with-rider-asset-pack) by [Onfe](https://onfe.itch.io/) is licensed under a commercial license purchased on [itch.io](https://itch.io/).

#### Sounds
[Small glass bell ringing](https://freesound.org/people/sammycrerar98/sounds/410910/) by [sammycrerar98](https://freesound.org/people/sammycrerar98/) is licensed under [CC BY-NC 3.0](https://creativecommons.org/licenses/by-nc/3.0/legalcode).

[Chomp Chew Bite](https://freesound.org/people/bbrocer/sounds/382650/) by [bbrocer](https://freesound.org/people/bbrocer/) is licensed under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/legalcode).



## MIT License

Copyright (c) 2020 Johannes Koch

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
